FROM node:alpine
WORKDIR /app
COPY . .
RUN npm install -g dtsgenerator && dtsgen openapi.yaml > src/openapi/api.ts
RUN npm i
